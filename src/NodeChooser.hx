package ;

import flash.display.Bitmap;
import flash.display.Graphics;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import openfl.Assets;

/**
 * ...
 * @author gordev
 */
class NodeChooser extends Sprite
{
	static var TOP:Int = 300;
	static var LEFT:Int = 200;
	static var BUTTONS_COUNT:Array<Int> = [4, 8];
	static var BUTTONS_MARGIN:Int = 100;
	public static var NODES_COUNT:Int = 8;
	public static var BUTTONS_ICONS:Array<Array<String>> = [
	["img/info_big.png",
	"img/story_big.png",
	"img/puzzle_big.png",
	"img/special_big.png"],

	["img/info.png",
	"img/story.png",
	"img/puzzle.png",
	"img/special.png",
	"img/book.png",
	"img/gem.png",
	"img/xp.png",
	"img/special_story.png"]
	];
	public var currentNodeType:Int = 0;
	var buttons:Array<Sprite>;

	public function new() 
	{
		super();
		addEventListener(Event.ADDED_TO_STAGE, init);
	}
	
	function init(e:Event)
	{
		removeEventListener(Event.ADDED_TO_STAGE, init);
		var gr:Graphics = this.graphics;
		gr.beginFill(0x000000, 0.4);
		gr.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
		gr.endFill();
		buttons = [];
		for (row in 0...BUTTONS_COUNT.length) {
			for (column in 0...BUTTONS_COUNT[row]) {
				var button = new Sprite();
				var image = new Bitmap(Assets.getBitmapData(BUTTONS_ICONS[row][column]));
				button.addChild(image);
				image.x = - image.width * 0.5;
				image.y = - image.height * 0.5;
				addChild(button);
				button.x = column * BUTTONS_MARGIN + LEFT;
				button.y = row * BUTTONS_MARGIN + TOP;
				button.addEventListener(MouseEvent.CLICK, function(e) { 
					this.visible = false;
					currentNodeType = row * NODES_COUNT + column; 
				} );
			}
		}
	}
	
}