package ;

import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Graphics;
import flash.display.Shape;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.geom.Point;
import haxe.io.Bytes;
import haxe.Json;
import msignal.Signal.Signal0;
import sys.io.FileInput;
import sys.io.FileOutput;
import systools.Dialogs;
import flash.Lib;
import sys.io.File;
import msignal.Signal.Signal1;
/**
 * ...
 * @author gordev
 */
class View extends Sprite
{
	var background:Bitmap;
	var backgroundFileName:String = "";
	var backgroundPath:String = "";
	var nodes:Array<NodeSpec>;
	var nodeChooser:NodeChooser;
	var startScreen:StartScreen;
	var firstNode:Int = -1;
	var connections:Array<ArcSpec>;
	var connectionsLayer:Shape;
	var currentConnection:Shape;
	var nodesHolder:Sprite;
	var moveNodeUp:Bool = false;
	var oldMoveCoords:Point;
	var movingNode:Node;
	public static var nodeClick = new Signal1<Int>();
	public static var deleteNode = new Signal1<Int>();
	public static var moveNode = new Signal1<Int>();
	public static var openProject = new Signal0();
	public static var newProject = new Signal1<String>();
	var projectName:String;

	public function new() 
	{
		super();
		addEventListener(Event.ADDED_TO_STAGE, init);
	}

	function init(e:Event)
	{
		this.graphics.beginFill(0xffffff);
		this.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
		this.graphics.endFill();
		connectionsLayer = new Shape();
		currentConnection = new Shape();
		nodes = [];
		connections = [];
		nodeChooser = new NodeChooser();
		startScreen = new StartScreen();
		nodesHolder = new Sprite();
		oldMoveCoords = new Point();
		nodeClick.add(onNodeClick);
		deleteNode.add(onNodeDelete);
		moveNode.add(onNodeMove);
		openProject.add(loadMap);
		newProject.add(function (s) { projectName = s; } );
		removeEventListener(Event.ADDED_TO_STAGE, init);
		addEventListener(Event.ENTER_FRAME, update);
		addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);

		stage.addChild(connectionsLayer);
		stage.addChild(currentConnection);
		stage.addChild(nodesHolder);
		stage.addChild(nodeChooser);
		stage.addChild(startScreen);
		nodeChooser.visible = false;
	}

	function update(e:Event)
	{
		if (firstNode != -1) {
			currentConnection.graphics.clear();
			drawArc(firstNode, -1, currentConnection.graphics);
		}
	}

	//TODO: add nodeRemove
	function onNodeClick(id:Int)
	{
		if (firstNode == -1) {
			firstNode = id;
		} else {
			//TODO: check if exist
			connections.push( { first: firstNode, second: id } );
			drawArc(firstNode, id, connectionsLayer.graphics);
			currentConnection.graphics.clear();
			firstNode = -1;
		}
	}

	function onNodeDelete(id:Int)
	{
		var deletedId = id;
		for (index in 0...nodesHolder.numChildren) {
			trace("current = " + cast(nodesHolder.getChildAt(index), Node).id + "; deleting " + id);
			if (cast(nodesHolder.getChildAt(index), Node).id == id) {
				nodesHolder.getChildAt(index).visible = false;
				nodesHolder.removeChildAt(index);
				break;
			}
		}
		connectionsLayer.graphics.clear();
		cleanUpConnections(id);
		nodes.splice(id, 1);
		for (index in 0...nodesHolder.numChildren) {
			if(nodes[index].id > id) nodes[index].id -= 1;
			if(cast(nodesHolder.getChildAt(index), Node).id > id) cast(nodesHolder.getChildAt(index), Node).id -= 1;
		}
		for (connect in connections) drawArc(connect.first, connect.second, connectionsLayer.graphics);
	}

	function onNodeMove(id:Int)
	{
		trace("moving");
		if (movingNode != null) 
		{
			stage.removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
			movingNode.moving = false;
			movingNode = null;
		} else {
			oldMoveCoords.x = stage.mouseX;
			oldMoveCoords.y = stage.mouseY;
			movingNode = cast nodesHolder.getChildAt(id);
			movingNode.moving = true;
			stage.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
		}
	}

	function onKeyUp(e:KeyboardEvent)
	{
		if (startScreen.visible) return;
		switch(e.keyCode) {
			case 27:
				Lib.trace("Esc pressed");
				firstNode = -1;
				currentConnection.graphics.clear();
				nodeChooser.visible = false;
			case 80:
				showHelp();
			case 81:
				setBackground();
			case 82:
				loadMap();
			case 83:
				saveMap();
			case 84:
				chooseNode();
		}
	}

	//TODO: check if there is alresy node
	function onMouseUp(e:MouseEvent)
	{
		if (Main.paused || firstNode != -1) return;
		nodes.push( { id: nodes.length, x: e.stageX, y: e.stageY, type: nodeChooser.currentNodeType, assets: [] } );
		var node = new Node(nodes.length - 1, nodeChooser.currentNodeType);
		node.x = e.stageX;
		node.y = e.stageY;
		nodesHolder.addChild(node);
	}

	function onMouseMove(e:MouseEvent)
	{
		var dx = oldMoveCoords.x - e.stageX;
		var dy = oldMoveCoords.y - e.stageY;
		movingNode.x -= dx;
		movingNode.y -= dy;
		oldMoveCoords.x = e.stageX;
		oldMoveCoords.y = e.stageY;

		nodes[movingNode.id].x = movingNode.x;
		nodes[movingNode.id].y = movingNode.y;

		connectionsLayer.graphics.clear();
		for (connect in connections) drawArc(connect.first, connect.second, connectionsLayer.graphics);
	}

	function showHelp()
	{
		Dialogs.confirm("Help", "F2 - open background image\n" + 
			"F3 - open saved project\n" + 
			"F4 - save curren ptoject\n" + 
			"F5 - choose node type\n\n" + 
			"F9 - export nodes\n\n" + 
			"Click on stage to place node.\n" + 
			"Click on nodes for connection.\n" + 
			"Ctrl+click on node to delete.\n" + 
			"Shift+click on node to start movement \nand click to put it on new place.", false);
	}

	function setBackground()
	{
		Main.paused = true;
		var filters: FILEFILTERS = 
			{ count: 1
			, descriptions: ["Image files"]
			, extensions: ["*.png;*.jpg;"]
			};
		var result = Dialogs.openFile
			( "Select a background file please!"
			, "Please select file for background"
			, filters
			);
		if (result != null) {
			var path:String = result[0];
			assignBackground(path);
		}
		Main.paused = false;
	}

	function assignBackground(path:String):Void
	{
		if (background != null) {
				removeChild(background);
			}
			background = new Bitmap(BitmapData.load(path));
			addChild(background);
			backgroundPath = path;
			var separatorPosition:Int = path.lastIndexOf('/');
			var backSeparatorPosition:Int = path.lastIndexOf('\\');
			if(backSeparatorPosition > separatorPosition) {
				backgroundFileName = path.substr(backSeparatorPosition + 1);
			} else {
				backgroundFileName = path.substr(separatorPosition + 1);
			}
	}

	function loadMap()
	{
		var filters: FILEFILTERS = 
			{ count: 1
			, descriptions: ["Map files"]
			, extensions: ["*.json;"]
			};
		var result = Dialogs.openFile
			( "Open a project file please!"
			, "Please select file to open"
			, filters
			);
		if (result != null) {
			var path:String = result[0];
			var fin:FileInput = File.read(path);
			var projectContent = Std.string(fin.readAll());

			var projectData:ProjectData = Json.parse(projectContent);
			var separatorPosition:Int = path.lastIndexOf('/');
			var backSeparatorPosition:Int = path.lastIndexOf('\\');
			trace(backSeparatorPosition, separatorPosition);
			if (backSeparatorPosition > separatorPosition) {
				backgroundPath = path.substring(0, backSeparatorPosition + 1);
				projectName = path.substr(backSeparatorPosition + 1);
				projectName = projectName.substring(0, projectName.length - 5);
			} else {
				backgroundPath = path.substring(0, separatorPosition + 1);
				projectName = path.substr(separatorPosition + 1);
				projectName = projectName.substring(0, projectName.length - 5);
			}
			assignBackground(backgroundPath + projectData.backgroundFile);
			nodes = [];
			connections = [];
			nodesHolder.removeChildren();
			connectionsLayer.graphics.clear();
			var id:Int = 0;
			for (node in projectData.nodes) {
				nodes.push( { id: id, x: node.x, y: node.y, type: node.type, assets: node.assets } );
				var placed = new Node(id, node.type);
				placed.x = node.x;
				placed.y = node.y;
				nodesHolder.addChild(placed);
				id++;
			}
			var num:Int = 0;
			while (num < projectData.connections.length) {
				connections.push( { first: projectData.connections[num], second: projectData.connections[num + 1] } );
				num += 2;
			}
			for (connect in connections) drawArc(connect.first, connect.second, connectionsLayer.graphics);
			Main.paused = false;
		} else {
			projectName = "default";
			Dialogs.message("Openning error", "Default new project created", true);
		}
	}

	function saveMap()
	{
		var result = Dialogs.folder
			( "Select a folder where to save project"
			, "Save path"
			);
		if (result != null) {
			var fout:FileOutput = File.write(result + "/" + projectName + ".json", false);
			fout.writeString("{\n\"nodes\": [\n");
			for (nIndex in 0...nodes.length) {
				var node = nodes[nIndex];
				fout.writeString("{" + 
					" \"id\": " + node.id + "," +
					" \"x\": " + node.x + "," +
					" \"y\": " + node.y + "," +
					" \"type\": " + node.type + "," +
					" \"assets\": " + node.assets.toString() +
					(nIndex == (nodes.length - 1) ? " }\n" : " },\n"));
			}
			fout.writeString("],\n");
			fout.writeString("\"connections\": [");
			for (cIndex in 0...connections.length) {
				var connection = connections[cIndex];
				fout.writeString(connection.first + ", " + connection.second + (cIndex == (connections.length - 1) ? "" : ", "));
			}
			fout.writeString("],\n");
			fout.writeString("\"backgroundFile\": \"" + backgroundFileName + "\"\n");
			fout.writeString("}\n");
			fout.close();
		}
	}

	function chooseNode()
	{
		nodeChooser.visible = true;
	}

	//utils
	function cleanUpConnections(deletedId:Int = -1)
	{
		if (deletedId != -1) {
			var connectionIndex = 0;
			var out = false;
			while (out != true) {
				if (connectionIndex == connections.length) {
					out = true;
					continue;
				}
				if (connections[connectionIndex].first == deletedId || connections[connectionIndex].second == deletedId) {
					connections.splice(connectionIndex, 1);
				} else {
					connectionIndex++;
				}
			}
			for (connect in connections) {
				if (connect.first > deletedId) connect.first--;
				if (connect.second > deletedId) connect.second--;
			}
		}
	}

	function drawArc(first:Int, second:Int, layer:Graphics)
	{
		if (first == -1) return;
		layer.lineStyle(6, 0x000000);
		layer.moveTo(nodes[first].x, nodes[first].y);
		if (second == -1) {
			layer.lineTo(stage.mouseX, stage.mouseY);
		} else {
			layer.lineTo(nodes[second].x, nodes[second].y);
		}
	}

	function makeBitmapARGB( width : Int, height : Int, bytes : haxe.io.Bytes ) : flash.display.BitmapData {
		var bmp = new flash.display.BitmapData(width,height,true);
		var ba = bytes.getData();
		var p = 0;
		for( y in 0...height )
			for( x in 0...width ) {
				bmp.setPixel32( x , y , (cast(ba[p], Int)<<24) | (cast(ba[p+1], Int)<<16) | (cast(ba[p+2], Int)<<8) | cast(ba[p+3], Int) );
				p += 4;
			}

		return bmp;
	}
}

typedef ProjectData = {
	var nodes:Array<NodeSpec>;
	var connections:Array<Int>;
	var backgroundFile:String;
}

typedef NodeSpec = {
	?var id: Int;
	var x: Float;
	var y: Float;
	var type:Int;
	var assets: Array<String>;
}

typedef ArcSpec = {
	var first:Int;
	var second:Int;
}
