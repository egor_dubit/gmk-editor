package ;

import flash.display.Bitmap;
import flash.display.Sprite;
import flash.display.Graphics;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.text.TextField;
import flash.text.TextFieldType;
import flash.text.TextFormat;
import flash.text.TextFormatAlign;
import openfl.Assets;

/**
 * ...
 * @author gordev
 */
class StartScreen extends Sprite
{
	static var BUTTONS_Y:Int = 300;
	static var BUTTONS_MARGIN:Int = 200;
	static var TEXT_WIDTH:Int = 400;
	static var TEXT_Y:Int = 550;
	static var TEXT_DEFAULT:String = "default";
	var openProjectButton:Sprite;
	var newProjectButton:Sprite;
	var projectName:TextField;

	public function new() 
	{
		super();
		addEventListener(Event.ADDED_TO_STAGE, init);
	}

	function init(e:Event)
	{
		var gr:Graphics = this.graphics;
		gr.beginFill(0x000000, 0.4);
		gr.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
		gr.endFill();

		openProjectButton = new Sprite();
		var openIcon = new Bitmap(Assets.getBitmapData("img/open_btn.png"));
		openIcon.x = - openIcon.width * 0.5;
		openIcon.y = - openIcon.height * 0.5;
		openProjectButton.addChild(openIcon);

		newProjectButton = new Sprite();
		var newIcon = new Bitmap(Assets.getBitmapData("img/new_btn.png"));
		newIcon.x = - newIcon.width * 0.5;
		newIcon.y = - newIcon.height * 0.5;
		newProjectButton.addChild(newIcon);

		openProjectButton.y = newProjectButton.y = BUTTONS_Y;
		openProjectButton.x = stage.stageWidth * 0.5 - BUTTONS_MARGIN;
		newProjectButton.x = stage.stageWidth * 0.5 + BUTTONS_MARGIN;
		var format:TextFormat = new TextFormat();
		format.size = 21;
		format.align = TextFormatAlign.CENTER;
		projectName = new TextField();
		projectName.defaultTextFormat = format;
		projectName.setTextFormat(format);
		projectName.type = TextFieldType.INPUT;
		projectName.width = TEXT_WIDTH;
		projectName.y = TEXT_Y;
		projectName.x = stage.stageWidth * 0.5 - projectName.width * 0.5;
		projectName.border = true;
		projectName.text = TEXT_DEFAULT;
		addChild(openProjectButton);
		addChild(newProjectButton);
		addChild(projectName);
		Main.paused = true;

		openProjectButton.addEventListener(MouseEvent.MOUSE_UP, function(e) { View.openProject.dispatch(); visible = false; } );
		newProjectButton.addEventListener(MouseEvent.MOUSE_UP, function(e) { View.newProject.dispatch(projectName.text); visible = false; } );
	}
}