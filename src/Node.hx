package ;

import flash.display.Bitmap;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import openfl.Assets;

/**
 * ...
 * @author gordev
 */
class Node extends Sprite
{
	public var id:Int;
	public var moving:Bool = false;

	public function new(id:Int, type:Int) 
	{
		super();
		this.id = id;
		var row:Int = Math.floor(type / NodeChooser.NODES_COUNT);
		var column:Int = type - row * NodeChooser.NODES_COUNT;
		var image = new Bitmap(Assets.getBitmapData(NodeChooser.BUTTONS_ICONS[row][column]));
		addChild(image);
		image.x = - image.width * 0.5;
		image.y = - image.height * 0.5;

		addEventListener(Event.ADDED_TO_STAGE, init);
	}

	function init(e:Event)
	{
		removeEventListener(Event.ADDED_TO_STAGE, init);

		addEventListener(MouseEvent.CLICK, function(e:MouseEvent) {
			e.stopImmediatePropagation();
			if (Main.paused) return;
			if (moving) {
				View.moveNode.dispatch(id);
				return;
			}
			if (e.ctrlKey) {
				View.deleteNode.dispatch(id);
			} else if (e.shiftKey) {
				View.moveNode.dispatch(id);
			} else {
				View.nodeClick.dispatch(id);
			}
		});
	}
}